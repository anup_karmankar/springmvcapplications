package com.wn.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.wn.model.User;
import com.wn.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	/*  @RequestMapping(value = "/register",method = RequestMethod.GET)
	   public String register(Map<String, Object> map) {
		map.put("user",new User());
		return "redirect:user/register";
	*/
		
		@RequestMapping(value = "/register", method = RequestMethod.GET) 
		public ModelAndView register() { 
		//    model.addAttribute("user", new User()); 
		    return new ModelAndView("user/register","user", new User()); 
	
	
		}
	   @RequestMapping(value = "/create", method = RequestMethod.POST)
	   public String create(@Valid @ModelAttribute("user")User user,BindingResult result,Map<String, Object> map) {
		
		if(result.hasErrors()){
			return "user/register";
		}
		
		userService.create(user);
		return "redirect:/user/details/"+user.getUserId();
	}

	@RequestMapping(value = "/details/{userId}", method = RequestMethod.GET)
	public String details(@PathVariable("userId")int userId, Map<String, Object> map) {
		User user=userService.find(userId);
		System.out.println("userID "+userId);
	//	map.put("userId",user.getUserId());
		map.put("fname", user.getFname());
		map.put("lname", user.getLname());
		map.put("email", user.getEmail());
		map.put("mobile", user.getMobile());
		return "user/details";
	}
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map) {
		map.put("userList", userService.getAll());
		return "user/list";
	}

	@RequestMapping(value = "/edit/{userId}", method = RequestMethod.GET)
	public String edit(@PathVariable("userId")int userId, Map<String, Object> map) {
		User user=userService.find(userId);
		map.put("user", user);
		return "user/edit";
      }
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(User user,Map<String, Object> map) {
		userService.update(user);
		return "redirect:/user/details/"+user.getUserId();
	}

	@RequestMapping(value = "/delete/{userId}", method = RequestMethod.GET)
	public String delete(@PathVariable("userId")int userId,Map<String, Object> map) {
		     userService.delete(userId);
		return "redirect:/user/list";
	}
}