package com.wn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.wn.dao.UserDao;
import com.wn.model.User;

@Service("userService")
@Transactional(propagation=Propagation.SUPPORTS,rollbackFor=Exception.class)
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;
	
	public void create(User user) {
		userDao.create(user);
		
	}
    public User edit(int userId) {

		return userDao.edit(userId);
	}
     
    public void update(User user) {
		userDao.update(user);
		
	}

	public void delete(int userId) {
       		userDao.delete(userId);
	}

	public User find(int userId) {
		return userDao.find(userId);
	}

	public List<User> getAll() {
	
		return userDao.getAll();
	}
	
}
