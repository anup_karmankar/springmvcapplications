package com.wn.service;

import com.wn.model.Account;

public interface RegistrationService {

	 public boolean checkUser(String username);
		
	 public void create(Account account);
	   public void update(Account account);
	   public Account edit(int userId);
	   public void delete(int userId);
}
