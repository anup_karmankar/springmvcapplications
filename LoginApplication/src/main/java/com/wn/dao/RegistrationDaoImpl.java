package com.wn.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wn.model.Account;

@Repository("RegistrationDao")
public class RegistrationDaoImpl implements RegistrationDao{

	
	@Autowired
	SessionFactory sessionFactory;
	
	protected Session currentSession(){
		return sessionFactory.getCurrentSession();
	}

	public boolean checkUser(String username) {
		System.out.println("In Check login");
		Session session = sessionFactory.openSession();
		boolean userFound = false;
		// Query using Hibernate Query Language
		String SQL_QUERY = " from Account as acc where acc.username=:username";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter("username", username);
		List list = query.list();

		if ((list != null) && (list.size() > 0)) {
			userFound = true;
		}

	//	session.close();
		return userFound;
	    }

	
	public void create(Account account) {
		currentSession().save(account);
    //   currentSession().close();
	}

	public Account edit(int userId) {
	//	return find(userId);
		return null;
	}

	public void update(Account account) {
		currentSession().update(account);

	}

	public void delete(int userId) {
		Account account = new Account();
		// hibernate deletes objects by the primary key
		account.setUserId(userId);
		currentSession().delete(account);
	}



}
