package com.wn.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;


@Entity
@Table(name="account")
public class Account implements Serializable {

    @Id
	@GenericGenerator(name="generator", strategy="increment")
	@GeneratedValue(generator="generator")
	private int userId;
	

	@Size(min=3,max=10,message="Username length should be between 3 to 10 Characters")
	private String username;
	
	@Size(min=3,max=10,message="Password length should be between 3 to 10 Characters")
	private String password;

    @Size(min=3,max=10,message="ConfirmPassword can not be blank and should match with Password")
	String confirmpassword;

	@Email(regexp="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+"[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",
			         message="Email should be in format")
	private String email;
		
 
	public Account() {
		// TODO Auto-generated constructor stub
	}


	public Account(int userId, String username, String password,
			String confirmpassword, String email) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.confirmpassword = confirmpassword;
		this.email = email;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getConfirmpassword() {
		return confirmpassword;
	}


	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	@Override
	public String toString() {
		return "Account [userId=" + userId + ", username=" + username
				+ ", password=" + password + ", confirmpassword="
				+ confirmpassword + ", email=" + email + "]";
	}

	
}
