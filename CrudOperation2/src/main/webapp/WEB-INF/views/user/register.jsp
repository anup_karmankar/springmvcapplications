<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	
		<h2>User Registration form</h2>
	

	<%--  <form action="/CrudOperation2/user/details" method="POST">
		<table>
			<tr>
				<td>Fname::</td>
				<td><input type="text" name="fname"></td>

			</tr>
			<tr>
				<td>Lname::</td>
				<td><input type="text" name="lname"></td>

			</tr>
			<tr>
				<td>Email::</td>
				<td><input type="text" name="email"></td>

			</tr>
			<tr>
				<td>Mobile::</td>
				<td><input type="text" name="mobile"></td>
			</tr>
         	<tr>
				<td colspan="2" align="center"><input type="submit"
					value="add User" /></td>
			</tr>
         			
		</table>
	</form>
   --%>

    <spring:url value="/user/list" var="list"></spring:url>
    <a href="${list}">User List</a>
	
	<c:url var="action" value="/user/create"></c:url>

	<form:form method="POST" action="${action}" modelAttribute="user">
		<table>
	<tr>
				<td>Fname::</td>
				<td><input type="text" name="fname"></td>

			</tr>
			<tr>
				<td>Lname::</td>
				<td><input type="text" name="lname"></td>

			</tr>
			<tr>
				<td>Email::</td>
				<td><input type="text" name="email"></td>

			</tr>
			<tr>
				<td>Mobile::</td>
				<td><input type="text" name="mobile"></td>
			</tr>
         	<tr>
				<td colspan="2" align="center"><input type="submit"
					value="add User" /></td>
			</tr>
        	
		</table>
	</form:form>
</body>
</html>