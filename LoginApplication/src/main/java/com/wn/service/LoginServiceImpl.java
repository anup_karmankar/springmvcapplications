package com.wn.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.wn.dao.LoginDao;
import com.wn.model.Account;

@Service("LoginService")
@Transactional(propagation=Propagation.SUPPORTS,rollbackFor=Exception.class)

public class LoginServiceImpl implements LoginService{

	 @Autowired
	 private LoginDao loginDao;

	   public void setLoginDAO(LoginDao loginDAO) {
             this.loginDao = loginDao;
      }
     
      public boolean checkLogin(String userName, String userPassword){
             System.out.println("In Service class...Check Login");
             return loginDao.checkLogin(userName, userPassword);
      }
	
}
