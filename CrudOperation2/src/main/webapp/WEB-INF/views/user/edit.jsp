<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Edit User</h2>
<spring:url value="/user/list" var="list"></spring:url>
<a href="${list}">User List</a>

<c:url var="action" value="/user/update"></c:url>

	<form:form method="POST" action="${action}" modelAttribute="user">
		<table>
			<tr>
			     <form:hidden path="userId"/>
                  <td><form:label path="fname">First Name:</form:label></td>
                  <td><form:input path="fname"/></td>
			</tr>
			<tr>
                  <td><form:label path="lname">Last Name:</form:label></td>
                  <td><form:input path="lname"/></td>
			</tr>
			<tr>
                  <td><form:label path="email">Email:</form:label></td>
                  <td><form:input path="email"/></td>
			</tr>
			
						<tr>
                  <td><form:label path="mobile">Mobile:</form:label></td>
                  <td><form:input path="mobile"/></td>
			</tr>
		 	<tr>
				<td colspan="2"><input type="submit" value="Update User" /></td>
			</tr>
        	
		</table>
	</form:form>
</body>
</html>