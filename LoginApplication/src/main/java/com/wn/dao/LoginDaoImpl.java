package com.wn.dao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.catalina.ant.FindLeaksTask;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wn.model.Account;

@Repository("loginDao")
public class LoginDaoImpl implements LoginDao {

/*	@Resource(name = "sessionFactory")
	protected SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected Session getSession() {
		return sessionFactory.openSession();
	}
*/	
	@Autowired
	SessionFactory sessionFactory;
	
	protected Session currentSession(){
		return sessionFactory.getCurrentSession();
	}


	public boolean checkLogin(String userName, String password) {
		System.out.println("In Check login");
		Session session = sessionFactory.openSession();
		boolean userFound = false;
		// Query using Hibernate Query Language
		String SQL_QUERY = " from Account as acc where acc.username=:username and acc.password=:password";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter("username", userName);
		query.setParameter("password", password);
		List list = query.list();

		if ((list != null) && (list.size() > 0)) {
			userFound = true;
		}

	//	session.close();
		return userFound;
	    }

	

}
