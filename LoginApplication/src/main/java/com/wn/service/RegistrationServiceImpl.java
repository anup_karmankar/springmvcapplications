package com.wn.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.wn.dao.RegistrationDao;
import com.wn.model.Account;

@Service("RegistrationService")
@Transactional(propagation=Propagation.SUPPORTS,rollbackFor=Exception.class)

public class RegistrationServiceImpl implements RegistrationService{

	@Autowired
	public RegistrationDao registrationDao;
	
	

	public void setRegistrationDao(RegistrationDao registrationDao) {
		this.registrationDao = registrationDao;
	}


	public boolean checkUser(String username) {
        System.out.println("In Service class...Check Login");
        return registrationDao.checkUser(username);
	}

	
	public void create(Account account) {
		registrationDao.create(account);
    //   currentSession().close();
	}


	public void update(Account account) {
		// TODO Auto-generated method stub
		
	}

	public Account edit(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

	public void delete(int userId) {
		// TODO Auto-generated method stub
		
	}

}
