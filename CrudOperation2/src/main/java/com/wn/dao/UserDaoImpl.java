package com.wn.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wn.model.User;

@Repository("userDao")
public class UserDaoImpl implements UserDao {

	@Autowired
	SessionFactory sessionFactory;
	
	protected Session currentSession(){
		return sessionFactory.getCurrentSession();
	}

	public void create(User user) {
		currentSession().save(user);
		
	}

	public User edit(int userId) {
		return find(userId);
	}

	public void update(User user) {
		currentSession().update(user);;
		
	}
	public void delete(int userId) {
		User user = new User();
	    // hibernate deletes objects by the primary key
	    user.setUserId(userId);
	    currentSession().delete(user);
	}

	public User find(int userId) {
		return (User) currentSession().get(User.class, userId);
	}

//	@SuppressWarnings("unchecked")
	public List<User> getAll() {
		return currentSession().createCriteria(User.class).list();
	}

}
