<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
 
 <style>
    .error 
    {
        color: #ff0000;
        font-weight: bold;
    }
    </style>

</head>
<body>
		<h2>User Registration form</h2>
	 
    <p class="error">${message}</p>		
	
	<form:form method="POST" action="registrationform.html"  modelAttribute="account">
	<!-- commandName="account" -->
	
<%-- 	<form:errors path="*">
	</form:errors>
 --%>	
		<table>
		<tr>
			<td>User Name:</tr>
		<tr>
			<td><input type="text" name="username"/></td>
			<td><form:errors path="username" cssClass="error"></form:errors></td>
		</tr>

		<tr>
			<td>Password:</td>
		</tr>
		<tr>
			<td><input type="password" name="password"/></td>
			<td><form:errors path="password" cssClass="error"></form:errors></td>
			
		</tr>

		<tr>
			<td>Confirm Password:</td>
		</tr>
		<tr>
			<td><input type="password" name="confirmpassword"></td>
		   <td><form:errors path="confirmpassword" cssClass="error"></form:errors></td>
		</tr>

		<tr>
			<td>Email:</td>
		</tr>
		<tr>
			<td><input type="text" name="email"/></td>
		    <td><form:errors path="email" cssClass="error"></form:errors></td>
		</tr>
		<tr>
			<td><input type="submit" value="Submit" /></td>
		</tr>
	</table>
	</form:form>
    <spring:url value="loginform.html" var="login"></spring:url>
	<a href="${login}">login</a>
</body>
</html>