<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<h2>User detail</h2>
<body>
<spring:url value="/user/list" var="list"></spring:url>
<a href="${list}">User List</a>
	<table>
		<tr>
			<td>Fname:</td>
			<td>${fname}</td>
		</tr>

		<tr>
			<td>Lname:</td>
			<td>${lname}</td>
		</tr>
		<tr>
			<td>Email:</td>
			<td>${email}</td>
		</tr>
		<tr>
			<td>Mobile:</td>
			<td>${mobile}</td>
		</tr>
	</table>



</body>
</html>