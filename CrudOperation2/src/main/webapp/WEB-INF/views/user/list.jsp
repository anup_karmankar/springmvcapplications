<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<spring:url value="/user/register" var="register"></spring:url>
	<a href="${register}">register</a>

	<c:if test="${!empty userList}">
		<table>
			<tr>
				<th>ID</th>
				<th>FirstName</th>
				<th>LastName</th>
				<th>Email</th>
				<th>Mobile</th>
				<th>Setting</th>

			</tr>

			<c:forEach items="${userList}" var="user">
			<tr>
				<td><c:out value="${user.userId}"></c:out></td>
				<td><c:out value="${user.fname}"></c:out></td>
				<td><c:out value="${user.lname}"></c:out></td>
				<td><c:out value="${user.email}"></c:out></td>
				<td><c:out value="${user.mobile}"></c:out></td>
				<td>
				    <a href="<c:url value='/user/edit/${user.userId}'/>">edit</a> 
				    <a href="<c:url value='/user/details/${user.userId}'/>">details</a>
				    <a href="<c:url value='/user/delete/${user.userId}'/>">delete</a> 
				</td>
			</tr>
			</c:forEach>
		</table>
	</c:if>

</body>
</html>