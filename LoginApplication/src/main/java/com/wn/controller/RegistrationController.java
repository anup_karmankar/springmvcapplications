package com.wn.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.wn.model.Account;
import com.wn.service.RegistrationService;

@Controller
@RequestMapping("/registrationform.html")
public class RegistrationController {

	@Autowired
	public RegistrationService registrationService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView register() {
		// model.addAttribute("account", new account());
		return new ModelAndView("user/registrationform", "account",new Account());
	}

	@RequestMapping(method = RequestMethod.POST)
	public String create(@ModelAttribute("account") @Valid Account account,
			BindingResult result, ModelMap map) {

		System.out.println(account);
	
		// Verifying if information is same  input by user

		boolean userExists = registrationService.checkUser(account.getUsername());

		if (userExists == true) {
			map.put("message", "Username already exist try another username !");
            boolean error=false;
			return "user/registrationform";
		  } 
		else{
			boolean error = false;
			if (account.getUsername().isEmpty() || !(account.getUsername().length() > 3)) {
				// result.rejectValue("username", "error.username");
				error = true;
			}

		    if (account.getPassword().isEmpty() || !(account.getPassword().length() > 3)) {
				// result.rejectValue("password", "error.password");
				error = true;
			}

		    if (account.getConfirmpassword().isEmpty()|| !account.getConfirmpassword().equals(
							account.getPassword())) {
				// result.rejectValue("confirmpassword",
				// "error.confirmpassword");
				error = true;
			}
		    if (account.getEmail().isEmpty() || !account.getEmail().contains("@")) {
				//result.rejectValue("email", "error.email");
				error = true;
			}

			if (error==true) {
				return "user/registrationform";
			}
		}
		
		registrationService.create(account);
		map.addAttribute("Account", account);
		return "user/registrationsuccess";

	}
}
