package com.wn.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name="user")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private int userId;
	
  //@Size(min=2, max=15,message="please enter 2 to 15 char only")
	private String fname;

  //@Size(min=2, max=15,message="please enter 2 to 15 char only")
	private String lname;
	
  //@Email
	private String email;
	
  //@Size(min=2, max=15,message="please enter 2 to 15 digit only")
	private String mobile;

	public User() {
		// System.out.println("0-param constructors");
	}

	public User(int userId, String fname, String lname, String email,
			String mobile) {
		this.userId = userId;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.mobile = mobile;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
}
