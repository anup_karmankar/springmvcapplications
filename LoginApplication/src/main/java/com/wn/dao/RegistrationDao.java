package com.wn.dao;

import com.wn.model.Account;

public interface RegistrationDao {

	 public boolean checkUser(String username);
	 public void create(Account account);
	   public void update(Account account);
	   public Account edit(int userId);
	   public void delete(int userId);
	  
}
